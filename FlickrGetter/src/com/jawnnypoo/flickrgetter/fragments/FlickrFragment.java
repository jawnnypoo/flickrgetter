package com.jawnnypoo.flickrgetter.fragments;

import java.util.ArrayList;
import java.util.Vector;

import com.jawnnypoo.flickrgetter.R;
import com.jawnnypoo.flickrgetter.adapters.FlickrPagerAdapter;
import com.jawnnypoo.flickrgetter.data.FlickrImage;
import com.jawnnypoo.flickrgetter.loaders.BitmapLoader;
import com.jawnnypoo.flickrgetter.loaders.SearchLoader;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FlickrFragment extends Fragment implements LoaderCallbacks<ArrayList<FlickrImage>>{
	
	private static final String TAG = FlickrFragment.class.getSimpleName();
	public static final String FRAG_TAG = FlickrFragment.class.getSimpleName() + "_TAG";
	private static final int LOAD_SEARCH = 42;
	private static final String BASE_SEARCH_URL = "http://api.flickr.com/services/rest/?format=json&sort=random&method=flickr.photos.search&tags=";
	private static final String END_SEARCH_URL = "&tag_mode=all&api_key=0e2b6aaf8a6901c264acb91f151a3350&nojsoncallback=1";
	
	private static final String STATE_POSITION = "state_position";
	private static final String STATE_QUERY = "state_query";
	
	private ViewPager mViewPager;
	private int mViewPagerPosition = -1;
	private String mSearchQuery;
	//Text version of the query, to show the user
	private String mTextQuery;
	private ArrayList<FlickrImage> mImages;
	
	public static FlickrFragment newInstance() {
		FlickrFragment frag = new FlickrFragment();
		Bundle args = new Bundle();
		frag.setArguments(args);
		return frag;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState == null) {
			//First launch, search for kittens
			buildSearchQuery("kitten");
		} else {
			//Restore where we were in the view pager
			mViewPagerPosition = savedInstanceState.getInt(STATE_POSITION, -1);
			mTextQuery = savedInstanceState.getString(STATE_QUERY);
			updateActionBarText(mTextQuery);
			Log.d(TAG, "Current Position of View Pager: " + mViewPagerPosition);
		}
		mImages = new ArrayList<FlickrImage>();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_POSITION, mViewPager.getCurrentItem());
		outState.putString(STATE_QUERY, mTextQuery);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_flickr, container, false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
		mViewPager.setAdapter(new FlickrPagerAdapter(getActivity(), mImages));
		if (mViewPagerPosition != -1 && mImages != null && mImages.size() < mViewPagerPosition) {
			mViewPager.setCurrentItem(mViewPagerPosition);
		}
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//Will either create a new loader and get the results, or obtain the previous data
		startSearchLoader();
	}
	
	public void updateQueryAndStartSearch(String query) {
		buildSearchQuery(query);
		restartSearchLoader();
	}
	
	private void startSearchLoader() {
		Log.d(TAG, "Loader started");
		getLoaderManager().initLoader(LOAD_SEARCH, null, this);
	}
	
	private void restartSearchLoader() {
		Log.d(TAG, "Loader restarted");
		mViewPagerPosition = 0;
		getLoaderManager().restartLoader(LOAD_SEARCH, null, this);
	}

	public void buildSearchQuery(String query) {
		mTextQuery = query;
		updateActionBarText(mTextQuery);
		//Replaces spaces with _
		query = query.replaceAll(" ", "_");
		mSearchQuery = BASE_SEARCH_URL + query + END_SEARCH_URL;
	}
	
	private void updateActionBarText(String text) {
		ActionBar actionBar = getActivity().getActionBar();
		actionBar.setTitle(getActivity().getString(R.string.search_for) + " " + text);
	}
	
	public void downloadImage() {
		if (mViewPager.getCurrentItem() < mImages.size()) {
			FlickrImage image = mImages.get(mViewPager.getCurrentItem());
			new BitmapLoader(image.generateFullURL(), image.getID(), getActivity()).execute();
		}
		
	}

	@Override
	public Loader<ArrayList<FlickrImage>> onCreateLoader(int id, Bundle args) {
		Log.d(TAG, "onCreateLoader called");
		if (mSearchQuery == null) {
			Log.e(TAG, "Created loader with no search term set");
			return null;
		}
		Loader<ArrayList<FlickrImage>> loader = new SearchLoader(getActivity(), mSearchQuery);
		loader.forceLoad(); //makes sure it loads. Based on bug from Async task
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<ArrayList<FlickrImage>> loader, ArrayList<FlickrImage> data) {
		
		if (data == null) {
			Log.e(TAG, "Loader returned no data");
			return;
		}

		switch (loader.getId()) {
		case LOAD_SEARCH:
			if (mImages != data) {
				mImages = data;
				Log.d(TAG, "Got data. Count: " + mImages.size());
				FlickrPagerAdapter adapter = (FlickrPagerAdapter) mViewPager.getAdapter();
				adapter.setImages(mImages);
				// Restore the position if it was there before
				if (mViewPagerPosition != -1 && mViewPagerPosition < mImages.size()) {
					mViewPager.setCurrentItem(mViewPagerPosition);
				}
				mViewPager.getAdapter().notifyDataSetChanged();
			}
			break;
		}
		
	}

	@Override
	public void onLoaderReset(Loader<ArrayList<FlickrImage>> loader) {
		switch (loader.getId()) {
		case LOAD_SEARCH:
			Log.d(TAG, "Loader Reset");
			mImages.clear();
			break;
		}
	}

}
