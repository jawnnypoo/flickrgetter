package com.jawnnypoo.flickrgetter;

import com.jawnnypoo.flickrgetter.fragments.FlickrFragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

public class FlickrActivity extends Activity implements OnQueryTextListener{
	private static final String TAG = FlickrActivity.class.getSimpleName();
	private FlickrFragment mFlickrFragment;
	private MenuItem mSearchMenuItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_root);
		mFlickrFragment = (FlickrFragment) getFragmentManager().findFragmentByTag(FlickrFragment.FRAG_TAG);
        
        if (mFlickrFragment == null) {
            mFlickrFragment = FlickrFragment.newInstance();
            getFragmentManager().beginTransaction().add(R.id.root, mFlickrFragment, FlickrFragment.FRAG_TAG).commit();
        }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		
		// Associate searchable configuration with the SearchView
	    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	    mSearchMenuItem = menu.findItem(R.id.search);
	    
	    SearchView searchView =(SearchView) mSearchMenuItem.getActionView();
	    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
	    searchView.setOnQueryTextListener(this);
		return true;
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) 
		{
		case R.id.download:
			if (mFlickrFragment != null) {
				mFlickrFragment.downloadImage();
			}
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		handleIntent(intent);
	}
	
	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d(TAG, "Searched For: " + query);
            //Pass off the search term to the fragment
            if (mFlickrFragment != null) {
            	mFlickrFragment.updateQueryAndStartSearch(query);
            }
        }
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		mSearchMenuItem.collapseActionView();
		return false;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		return false;
	}

}
