package com.jawnnypoo.flickrgetter.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jawnnypoo.flickrgetter.R;
import com.jawnnypoo.flickrgetter.data.FlickrImage;
import com.squareup.picasso.Picasso;

public class FlickrPagerAdapter extends PagerAdapter{

	ArrayList<FlickrImage> mImages;
	Context mContext;
	LayoutInflater mLayoutInflater;
	
	public FlickrPagerAdapter(Context context, ArrayList<FlickrImage> images) {
		mContext = context;
		mImages = images;
		mLayoutInflater = LayoutInflater.from(mContext);
	}
	
	public void setImages(ArrayList<FlickrImage> images) {
		mImages = images;
	}
	
	@Override
	public int getCount() {
		if (mImages == null) {
			return 0;
		} else {
			return mImages.size();
		}
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View view = mLayoutInflater.inflate(R.layout.item_flickr_page, container, false);
		FlickrImage flickrImage = mImages.get(position);
		container.addView(view);
		
		ImageView image = (ImageView) view.findViewById(R.id.flickr_image);
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(flickrImage.getTitle());
		Picasso.with(mContext).load(flickrImage.generateFullURL()).into(image);
		view.setTag(flickrImage);
		return view;
		
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	
	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

	

}
