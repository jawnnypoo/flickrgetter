package com.jawnnypoo.flickrgetter.data;

import java.io.Serializable;

//This class is there to store all of the parsed JSON data for each image
public class FlickrImage implements Serializable{
	private static final long serialVersionUID = 4041804997103709097L;
	private String mID;
	private String mOwner;
	private String mSecret;
	private String mServer;
	private String mFarm;
	private String mTitle;
	private int misPublic;
	private int misFriend;
	private int misFamily;
	
	public FlickrImage(String id, String owner, String secret, String server, String farm, String title, int pub, int friend, int family) {
		mID = id; mOwner = owner; mSecret = secret; mServer = server; mFarm = farm; mTitle = title; misPublic = pub; misFriend = friend; misFamily = family;
	}
	public String generateMobileURL() {
		//Formulate the URL based on the recieved JSON values
		String returnURL = "http://farm" + mFarm + ".static.flickr.com/" + mServer + "/" + mID + "_" + mSecret + "_m.jpg";
		
		return returnURL;
	}
	
	//Return the URL without the m tag for higher resolution downloads
	public String generateFullURL() {
		String returnURL = "http://farm" + mFarm + ".static.flickr.com/" + mServer + "/" + mID + "_" + mSecret + ".jpg";
		return returnURL;
	}
	
	public String getTitle() {
		return mTitle;
	}
	
	public String getID() {
		return mID;
	}

}
