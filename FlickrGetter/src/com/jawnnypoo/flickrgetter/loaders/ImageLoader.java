package com.jawnnypoo.flickrgetter.loaders;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.animation.Animation;
import android.widget.ImageView;

/**
 * Loads an image in a very simple way. 
 * @author john.carlson
 *
 */
public class ImageLoader extends AsyncTask<String, Void, Bitmap> {
		//Represents our imageview from the main actiiv
	    ImageView bmImage;
	    //Pass in the image reference
	    public ImageLoader(ImageView bmImage) {
	        this.bmImage = bmImage;
	    }

	    //Pull the bitmap from the specified URL
	    protected Bitmap doInBackground(String... urls) {
	        String urldisplay = urls[0];
	        Bitmap mImageBM = null;
	        try {
	            InputStream in = new java.net.URL(urldisplay).openStream();
	            mImageBM = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            Log.e("Error: ", e.getMessage());
	        }
	        return mImageBM;
	    }

	    protected void onPostExecute(Bitmap result) {
	        bmImage.setImageBitmap(result);
	    }
}

