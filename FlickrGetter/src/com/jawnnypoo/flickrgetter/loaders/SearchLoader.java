package com.jawnnypoo.flickrgetter.loaders;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Vector;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.jawnnypoo.flickrgetter.data.FlickrImage;

//The AsyncTaskLoader that parses the JSON into URLs from the search results
public class SearchLoader extends AsyncTaskLoader<ArrayList<FlickrImage>> {
	private static final String TAG = SearchLoader.class.getSimpleName();
	private static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Linux; Android " + Build.VERSION.RELEASE + "; " + Build.MODEL + " Build/" + Build.DISPLAY +")";
	
	private static final int BUFFER_SIZE = 4098;
	
	private static final String FEED_PHOTOS = 	"photos";
	private static final String FEED_PHOTO = 	"photo";
	private static final String FEED_FARM = 	"farm";
	private static final String FEED_ID = 		"id";
	private static final String FEED_PUBLIC = 	"ispublic";
	private static final String FEED_FAMILY = 	"isfamily";
	private static final String FEED_FRIEND = 	"isfriend";
	private static final String FEED_OWNER = 	"owner";
	private static final String FEED_SECRET = 	"secret";
	private static final String FEED_SERVER = 	"server";
	private static final String FEED_TITLE = 	"title";
	
	private String mFeedURL;
	
	public SearchLoader(Context context, String feedURL) {
		super(context);
		mFeedURL = feedURL;
	}

	@Override
	public ArrayList<FlickrImage> loadInBackground() {
		try{
			String feed = downloadJSON(new URL(mFeedURL));
			ArrayList<FlickrImage> newImages = parse(feed);
			
			return newImages;
			
		} catch(MalformedURLException e){
			Log.e(TAG,"URL Exception", e);
			
		}
		return null;
	}

	
	//Download the JSON from the URL
	private String downloadJSON(URL url) {
		Log.d(TAG, "downloading JSON");
		
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		String result = null;

		InputStream inStream = null;
		try {
			URLConnection urlConnection = url.openConnection();
			if (urlConnection instanceof HttpURLConnection) {
				HttpURLConnection connection = (HttpURLConnection) urlConnection;
				connection.setRequestProperty(HTTP.USER_AGENT, DEFAULT_USER_AGENT);
				connection.connect();
				inStream = connection.getInputStream();
				byte buffer[] = new byte[BUFFER_SIZE];
				int bytesRead = 0;
				while ((bytesRead = inStream.read(buffer)) >= 0) {
					if (bytesRead > 0) 
					{
						outStream.write(buffer, 0, bytesRead);
					}
				}

				try {
					result = new String(outStream.toByteArray(), HTTP.UTF_8);
				} catch (UnsupportedEncodingException e) {
					Log.e(TAG, "Cannot create string", e);
				}
			}
		} catch (IOException e) {
			Log.e(TAG, "IOException during download", e);
		} finally {
			try {
				if (inStream != null) {
					inStream.close();
				}
				outStream.close();
			} catch (IOException e) {
				Log.e(TAG, "Failed to close input stream", e);
			}
		}

		return result;
	}
	
	//Parse the JSON we have received
	public ArrayList<FlickrImage> parse(String feed) {
		Log.d(TAG, "parsing JSON");
		ArrayList<FlickrImage> vectorURLs = new ArrayList<FlickrImage>();
		if (!TextUtils.isEmpty(feed)) {
			try {
				JSONObject root = new JSONObject(feed);
				JSONObject photo = root.getJSONObject(FEED_PHOTOS);
				JSONArray photoArray = photo.getJSONArray(FEED_PHOTO);
				
				for (int i = 0; i < photoArray.length(); i++) {
					JSONObject photoObject = photoArray.getJSONObject(i);
					//Build a new image object out of the parsed JSON
					FlickrImage image = new FlickrImage(
							photoObject.getString(FEED_ID),
							photoObject.getString(FEED_OWNER),
							photoObject.getString(FEED_SECRET),
							photoObject.getString(FEED_SERVER),
							photoObject.getString(FEED_FARM),
							photoObject.getString(FEED_TITLE),
							photoObject.getInt(FEED_PUBLIC),
							photoObject.getInt(FEED_FRIEND),
							photoObject.getInt(FEED_FAMILY)
							);
					vectorURLs.add(image);
				}
			} catch (JSONException e) {
				Log.e(TAG, "Parse failed \n" + feed + "\n", e);
				return null;
			}
		}
		//Return the vector of all of the images we created
		return vectorURLs;	
	}

}
