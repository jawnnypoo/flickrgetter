package com.jawnnypoo.flickrgetter.loaders;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.jawnnypoo.flickrgetter.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.Toast;

//The class that pulls the image data in the background and returns the correct
//image in the imageview
public class BitmapLoader extends AsyncTask<String, Void, Bitmap> {
		//Represents our imageview from the main activity
	    String mID;
	    String mURL;
	    String mDir;
	    boolean success = false;
	    Context mContext;
	    //Pass in the image reference
	    public BitmapLoader(String url, String id, Context context) {
	        this.mID = id;
	        this.mURL = url;
	        this.mContext = context;
	    }

	    //Pull the bitmap from the specified URL
	    @Override
		protected Bitmap doInBackground(String... params) {
			// TODO Auto-generated method stub
			Bitmap mImageBM = null;
	        try {
	            InputStream in = new java.net.URL(mURL).openStream();
	            mImageBM = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            Log.e("Error", e.getMessage());
	            e.printStackTrace();
	        }
	        //Still null? Failure to download
	        if (mImageBM == null) {
	        	success = false;
	        	return mImageBM;
	        }
	        
	        String root = Environment.getExternalStorageDirectory().toString();
	        File myDir = new File(root + "/" + mContext.getString(R.string.app_name));    
	        myDir.mkdirs();
	        mDir = myDir.toString();
	        
	        File file = new File (myDir, mID+ ".jpg");
	        if (file.exists ()) { file.delete (); } 
	        try {
	               FileOutputStream out = new FileOutputStream(file);
	               mImageBM.compress(Bitmap.CompressFormat.JPEG, 100, out);
	               out.flush();
	               out.close();
	               success = true;
	        } catch (Exception e) {
	               e.printStackTrace();
	               success = false;
	        }
	        
	        return mImageBM;
		}

	    @Override
	    protected void onPreExecute() {
	    	super.onPreExecute();
	    	Toast.makeText(mContext, "Downloading " + mID + ".jpg...", Toast.LENGTH_SHORT).show();
	    }
	    protected void onPostExecute(Bitmap result) {
	        Log.d("BML", "Saved image to SD");
	        if (success) {
	        	Toast.makeText(mContext, "Downloaded image to " + mDir, Toast.LENGTH_LONG).show();
	        }
	        else { 
	        	Toast.makeText(mContext, "Failed to download image", Toast.LENGTH_LONG).show();
	        }
	    }		
}

