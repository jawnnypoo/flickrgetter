# README #

This small project allows the user to search for photos on Flickr using their search API, along with custom loaders and JSON parsing to get the results. It also takes advantage of Picasso for downloading the images to display, with a custom loader to download images to store on the device. 


